import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  /*
  param1: string = "";
  param2: string = "";
  param3: string = "";
  param4: string = "";
  param5: string = "";

  miFuncionRefencia(dato1: string, dato2: string, dato3: string, dato4: string, dato5: string) {
    console.log(dato1);
  }

  miFuncionNgModel() {
    console.log(this.param1);
  }
  */

  nombre: FormControl = new FormControl('Yrving', Validators.required);

  miFormulario: FormGroup = new FormGroup({
    nombres: new FormControl('', Validators.required),
    apellidos: new FormControl('', Validators.required),
    edad: new FormControl('', [Validators.required, Validators.min(18)]),
    correo: new FormControl('', [Validators.required, Validators.email]),
  });

}
